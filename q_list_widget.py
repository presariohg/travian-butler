from PyQt5.QtWidgets    import  QListWidget, QListWidgetItem, QAbstractItemView
from PyQt5.QtCore       import  pyqtSignal, Qt

class Q_List_Widget(QListWidget):
    itemMoved           =   pyqtSignal(int, int, QListWidgetItem) # Old index, new index, item
    itemRightClicked    =   pyqtSignal()

    def __init__(self, parent   =   None, **args):
        super(Q_List_Widget, self).__init__(parent, **args)
        self.setAcceptDrops(True)
        self.setDragEnabled(True)
        self.setDragDropMode(QAbstractItemView.InternalMove)
        self.drag_item      =   None
        self.drag_row       =   None

    def dropEvent(self, event):
        super(Q_List_Widget, self).dropEvent(event)
        self.itemMoved.emit(self.drag_row, self.row(self.drag_item), self.drag_item)
        self.drag_item      =   None
        self.setCurrentRow(-1)

    def startDrag(self, supportedActions):
        self.drag_item      =   self.currentItem()
        self.drag_row       =   self.row(self.drag_item)
        super(Q_List_Widget, self).startDrag(supportedActions)

    def mousePressEvent(self, event):
        if (event.button()  ==  Qt.RightButton):
            super(Q_List_Widget, self).mousePressEvent(event)
            self.itemRightClicked.emit()
        else:
            super(Q_List_Widget, self).mousePressEvent(event)
            self.setCurrentRow(-1)


#drag & drop credit:    Nathan Weston                   nathan at genarts.com
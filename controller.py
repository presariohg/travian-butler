from view import View
from model import *

import sys
import requests
import os

from PyQt5.QtWidgets import *
from PyQt5.uic import *
from PyQt5.QtCore import *

from time import *
from collections import *

class List_Widget_Item(QListWidgetItem):
	def __init__(self, building):
		super(List_Widget_Item, self).__init__()
		self.setText(building.name + ' level ' + str(building.level +  building.pending_level))
		self.building 	=	building

class Controller:
	mapper 					= 	QSignalMapper()
	one_sec_timer			=	QTimer()
	time_counter_in_sec		=	0
	is_confirmed_build		=	False

	def __init__(self, model, view):
		self.model 			= 	model
		self.view  			= 	view
		self.model.login()

		self.view.bt_quit.clicked.connect(self.show_quit_confirm_box)
		self.view.bt_refresh.clicked.connect(self.refresh)
		self.view.bt_upgrade.clicked.connect(self.upgrade_building)
		# self.view.bt_unnamed.clicked.connect(self.unnamed)
		self.view.lw_task_list.itemRightClicked.connect(self.view.task_list_remove_item)

		for i in range (0, 40):
			self.view.lb_building[i].clicked.connect(self.mapper.map)
			self.mapper.setMapping(self.view.lb_building[i], i)
		self.mapper.mapped.connect(self.update_task_list)

		self.refresh()

	def show_quit_confirm_box(self):
		cmd 		= 	QMessageBox.question(self.view, 'Confirm', 'Wanna quit already?')

		if (cmd 	== 	QMessageBox.Yes):
			QApplication.quit()

#######################################################

	def refresh(self):

		page	=	self.model.rq.get(self.model.URL_OUTER)
		if ('Incoming attacks to me' in page.text):
			duration = 0.5
			freq = 440
			os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))

		self.model.update_building_list_info(page_outer = page)
		self.model.update_current_built_info(page)
		self.model.update_resources_info(page)
		self.model.update_adventure(page)

		if ('in home village' in page.text):
			if (self.model.new_adventures > 0):
				self.model.go_adventure()

		self.view.update_combobox(self.model.building_list)
		self.view.update_label_building_list(self.model.building_list)
		self.view.update_progressbar_resources(self.model.storage, self.model.capacity)
		self.view.update_label_adventure(self.model.new_adventures)

	def update_task_list(self, index):
		self.model.building_list[index].pending_level += 1
		self.view.update_task_list(List_Widget_Item(self.model.building_list[index]))

#######################################################

	def one_sec_timer_interrupt(self):
		self.model.update_storage()
		self.view.update_progressbar_resources(self.model.storage, self.model.capacity)

		if (len(self.model.current_built_list) != 0): 
			if (self.model.current_built_list[0].duration_in_sec 	> 	0):
				for building in self.model.current_built_list:
					building.duration_in_sec	-=	1

			else:
				self.model.current_built_list.popleft()

				for building in self.model.current_built_list:
					building.duration_in_sec	-=	1

			self.view.update_table_current_built(self.model.current_built_list)
		
		if ((self.view.tw_current_built.item(0,0) == None) and
			(self.view.lw_task_list.item(0) != None)):
			self.pop_queue()
		
		self.time_counter_in_sec		+=	1

		if (self.time_counter_in_sec	==	10):
			self.refresh()
			self.time_counter_in_sec	=	0

#######################################################

	def pop_queue(self):
		self.view.lw_task_list.takeItem(0).building.upgrade_one_level(self.model.rq, self.model.URL_ORIGIN)
		self.refresh()

	def upgrade_building(self):
		this_building 	= 	self.model.building_list[self.view.cb_building_list.currentIndex()]
		for i in range(20):
			self.update_task_list(this_building.id - 1)

	def new_village_init(self):
		for i in range(19):
			self.update_task_list(25)

		for i in range(5):
			for j in range(18):
				self.update_task_list(j)

app			=	QApplication(sys.argv)
model		=	Model()
view		=	View()
controller	=	Controller(model, view)

controller.view.show()

controller.one_sec_timer.timeout.connect(controller.one_sec_timer_interrupt)
controller.one_sec_timer.start(1000)


sys.exit(app.exec_())

#TODO: SO TAN LINH
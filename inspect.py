import re

class Building(object):
	def __init__(self, name = '', level = 0, id = '', url = ''):
		self.name 	= 	name
		self.level	=	level
		self.id		=	id
		self.url	=	url


def get_build_token (location, page_content):

	if (location == "outer"):
		result = re.findall(r"(?<=')dorf1.php.+?(?=')", page_content)
	else:
		result = re.findall(r"(?<=')dorf2.php.+?(?=')", page_content)

	return result[0].replace("&ins","")

def get_build_time (page_content):
	result = re.findall(r"alt=\"duration\">\n(.+?) </span>", page_content)
	time = result[0].split(':')
	time_in_sec = int(time[0]) * 60 * 60 + int(time[1]) * 60 + int(time[2])
	return time_in_sec + 1

####################################

def get_building_info (page_content):
	#co nen dung class?
	result = re.findall(r'titleInHeader">(.*?)</span>', page_content)
	name = re.findall(r'(.+?) <span', result[0])
	level = re.findall(r'level (\d+)',result[0])
	info = dict(name = name[0], level = level[0])
	return(info)

def get_all_building_info (page_content):
	outer_buildings = []

	raw_data 		=	re.findall(r'<area(.+?)/>', page_content, re.DOTALL)

	for line in raw_data:
		if ('data-index' not in line): #last <area> tag in dorf1.php
			continue
		elif ('Building site' in line):
			name 	=	'Building site'
			level	=	0
		else:
			name		=	re.findall(r'title="(.+?)&amp;', line)[0]
			level		=	re.findall(r'level (.+?)&lt;', line)[0]

		id 			= 	re.findall(r'data-index="(.+?)"', line)[0]
		url			=	re.findall(r'href="(.+?)"', line)[0]

		outer_buildings.append(Building(name, level, id, url))

	return outer_buildings

####################################

def get_demolish_data (building_id, page_content):
	a 				= 	re.findall(r'name="a" value="(.+?)"', page_content)
	c 				= 	re.findall(r'name="c" value="(.+?)"', page_content) 
	data_demolish 	= 	dict(	gid = 15,
								a = a[0],
								c = c[0],
								abriss = building_id)
	return data_demolish

####################################

def get_send_troops_data (t1_amount, dname, option, page_content):

	timestamp 			= 	re.findall(r'name="timestamp" value="(\d+)', 			page_content)
	timestamp_checksum 	= 	re.findall(r'name="timestamp_checksum" value="(.+?)"',	page_content)
	b 					= 	re.findall(r'name="b" value="(.+?)"', 					page_content)
	current_did 		= 	re.findall(r'name="currentDid" value="(.+?)"', 			page_content)
	mpvt_token 			= 	re.findall(r'name="mpvt_token" value="(.+?)"', 			page_content)

	if (option == "raid"):
		c = 4
	elif (option == "attack"):
		c = 3
	elif (option == "reinforcement"):
		c = 2

	data_send_troops 	= 	dict(	timestamp 			= 	timestamp[0], 
									timestamp_checksum 	= 	timestamp_checksum[0],
									b 					= 	b[0],
									currentDid 			= 	current_did[0],
									mpvt_token 			= 	mpvt_token[0],
									t1 					= 	2000000,
									t4 					= 	'',
									t7 					= 	'',
									t9 					= 	'',
									t2 					= 	'',
									t5 					= 	'',
									t8 					= 	'',
									t10 				= 	'',
									t3					= 	'',
									t6 					= 	'',
									t11 				= 	'',
									dname 				= 	dname,
									x					= 	'',
									y 					= 	'',
									c 					= 	c,
									s1 					= 	'ok')

	return data_send_troops

def get_send_troops_confirm_data (page_content):
	# redeploy_hero 		= 	re.findall(r"name=\"redeployHero\" value=\"(.*?)(?=\" )", 		page_content)
	timestamp 			= 	re.findall(r'name="timestamp" value="(.+?)"', 			page_content)
	timestamp_checksum	= 	re.findall(r'name="timestamp_checksum" value="(.+?)"', 	page_content)
	# ID 					= 	re.findall(r'name="id" value="(.+?)"', 					page_content)
	a 					= 	re.findall(r'name="a" value="(.+?)"', 					page_content)
	c 					= 	re.findall(r'name="c" value="(.+?)"', 					page_content)
	kid 				= 	re.findall(r'name="kid" value="(.+?)"', 				page_content)
	t1 					= 	re.findall(r'name="t1" value="(.+?)"', 					page_content)
	t2 					= 	re.findall(r'name="t2" value="(.+?)"', 					page_content)
	t3 					= 	re.findall(r'name="t3" value="(.+?)"', 					page_content)
	t4 					= 	re.findall(r'name="t4" value="(.+?)"', 					page_content)
	t5 					= 	re.findall(r'name="t5" value="(.+?)"', 					page_content)
	t6 					= 	re.findall(r'name="t6" value="(.+?)"', 					page_content)
	t7 					= 	re.findall(r'name="t7" value="(.+?)"', 					page_content)
	t8 					= 	re.findall(r'name="t8" value="(.+?)"', 					page_content)
	t9 					= 	re.findall(r'name="t9" value="(.+?)"', 					page_content)
	t10 				= 	re.findall(r'name="t10" value="(.+?)"', 				page_content)
	t11 				= 	re.findall(r'name="t11" value="(.+?)"', 				page_content)
	send_really 		= 	re.findall(r'name="sendReally" value="(.+?)"', 			page_content)
	troop_sent 			= 	re.findall(r'name="troopsSent" value="(.+?)"', 			page_content)
	current_did 		= 	re.findall(r'name="currentDid" value="(.+?)"', 			page_content)
	b 					= 	re.findall(r'name="b" value="(.+?)"', 					page_content)
	dname 				= 	re.findall(r'name="dname" value="(.*?)"', 				page_content)
	x 					= 	re.findall(r'name="x" value="(.+?)"', 					page_content)
	y 					= 	re.findall(r'name="y" value="(.+?)"', 					page_content)
	
	data_confirm		= 	dict(	redeployHero 		= 	'',
									# redeployHero 		= 	redeploy_hero[0],
									timestamp 			=	timestamp[0],
									timestamp_checksum 	= 	timestamp_checksum[0],
									id 					= 	39,
									a 					= 	a[0],
									c 					= 	c[0],
									kid 				= 	kid[0],
									t1 					= 	t1[0],
									t2 					= 	t2[0],
									t3 					= 	t3[0],
									t4 					= 	t4[0],
									t5 					= 	t5[0],
									t6 					= 	t6[0],
									t7 					= 	t7[0],
									t8 					= 	t8[0],
									t9 					= 	t9[0],
									t10 				= 	t10[0],
									t11 				= 	t11[0],
									sendReally 			= 	send_really[0],
									troopsSent 			= 	troop_sent[0],
									currentDid 			= 	current_did[0],
									b 					=	b[0],
									dname 				= 	dname[0],
									x 					= 	x[0],
									y 					= 	y[0],
									s1 					= 	'ok')

	return data_confirm

####################################

def get_smithy_info (page_content):
	smithy_info = dict()

	result = re.findall(r'<div class="title">(.+?)</span>', page_content, re.DOTALL)
	for raw in result:
		name = re.findall(r'title="(.+?)"', raw)
		level = re.findall(r'level (\d+)', raw)
		smithy_info[name[0]] = level[0]

	return smithy_info

def get_smithy_token (page_content):
	result = re.findall(r"build\.php\?id=23&a=.+(?=')", page_content)
	#build.php?id=23&a=1&c=a8a4f3b4
	return(result[0])

####################################

def get_adventure_token (page_content): 
	
	token 	= 	re.findall(r'class="gotoAdventure arrow" href="(.+?)"', page_content)
	#<a class="gotoAdventure arrow" href="start_adventure.php?skip=1&amp;from=list&amp;kid=86496">To the adventure.</a>
	time 	= 	re.findall(r'<td class="moveTime"> (.+?) </td>', page_content)
	
	return(token[0].replace("&amp;","&"))

def get_adventure_data (page_content):
	send 	= 	re.findall(r'name="send" value="(.+?)"', page_content)
	kid 	= 	re.findall(r'name="kid" value="(.+?)"', page_content)
	a 		= 	re.findall(r'name="a" value="(.+?)"', page_content)

	adventure_data = dict(	send 	= 	send[0], 
							kid 	= 	kid[0],
							a 		= 	a[0], 
							start 	= 'Start adventure')
	adventure_data['from'] = 'list'
	
	return adventure_data

####################################

def get_village_list (server_address, page_content):
	id_village			=	re.findall(r'href="\?newdid=(.+?)"', page_content)
	url_village			=	[]

	for id in id_village:
		url_village.append("{}dorf2.php?newdid={}".format(server_address, id))

	return url_village

####################################

def get_resources_production_info (page_content):
	production 				=	{}

	raw_data				=	re.findall(r'resources.production = {\n(.+?)}', page_content, re.DOTALL)[0]

	production['lumber']	=	int (re.findall(r'"l1": (.+?),', raw_data)[0])
	production['clay'] 		=	int (re.findall(r'"l2": (.+?),', raw_data)[0])
	production['iron'] 		=	int (re.findall(r'"l3": (.+?),', raw_data)[0])
	production['crop'] 		=	int (re.findall(r'"l4": (.+?),', raw_data)[0])

	return production

def get_resources_capacity_info (page_content):
	capacity				=	{}

	raw_data				=	re.findall(r'resources.maxStorage = {\n(.+?)}', page_content, re.DOTALL)[0]

	capacity['lumber']		=	int (re.findall(r'"l1": (.+?),', raw_data)[0])
	capacity['clay'] 		=	int (re.findall(r'"l2": (.+?),', raw_data)[0])
	capacity['iron'] 		=	int (re.findall(r'"l3": (.+?),', raw_data)[0])
	capacity['crop'] 		=	int (re.findall(r'"l4": (.+?)$', raw_data)[0])

	return capacity

def get_resources_storage_info (page_content):
	storage				=	{}

	raw_data			=	re.findall(r'resources.storage = {\n(.+?)}', page_content, re.DOTALL)[0]

	storage['lumber']	=	int (re.findall(r'"l1": (.+?)\.', raw_data)[0])
	storage['clay']		=	int (re.findall(r'"l2": (.+?)\.', raw_data)[0])
	storage['iron']		=	int (re.findall(r'"l3": (.+?)\.', raw_data)[0])
	storage['crop']		=	int (re.findall(r'"l4": (.+?)\.', raw_data)[0])

	return storage

def get_build_duration(page_content):
	if ('buildDuration' in page_content):
		raw_data				=	re.findall(r'<div class="buildDuration">(.+?)</div>', page_content, re.DOTALL)[0]
		build_duration_in_sec 	=	int (re.findall(r'value="(.+?)"', raw_data)[0])

		return build_duration_in_sec
	else :
		return -1

def get_current_build_info (page_content):
	if ('buildDuration' in page_content):
		raw_data		=	re.findall(r'<div class="boxes buildingList">(.+?)<div class="buildDuration">', page_content, re.DOTALL)[0]

		name 			=	re.findall(r'<div class="name">\n(.+?)\n<span class="lvl">', raw_data, re.DOTALL)[0]
		level			= 	re.findall(r'<span class="lvl">(.+?)</span>', raw_data)[0]

		return name + ' ' + level

	else:
		return ''

def get_trade_data(page_content):
	raw_data 		=	re.findall(r'<div class="action first">(.+?)</script>', page_content, re.DOTALL)[0]
	cmd				=	'exchangeResources'

	tid				=	re.findall(r'"tid":"(.+?)"', raw_data)[0]
	nr				=	re.findall(r'"nr":(.+?),', raw_data)[0]
	btyp			=	re.findall(r'"btyp":(.+?),', raw_data)[0]
	r1				=	re.findall(r'"r1":(.+?),', raw_data)[0]
	r2				=	re.findall(r'"r2":(.+?),', raw_data)[0]
	r3				=	re.findall(r'"r3":(.+?),', raw_data)[0]
	r4				=	re.findall(r'"r4":(.+?),', raw_data)[0]
	supply			=	re.findall(r'"supply":(.+?),', raw_data)[0]
	pzeit			=	re.findall(r'"pzeit":(.+?),', raw_data)[0]
	max1			=	re.findall(r'"max1":(.+?),', raw_data)[0]
	max2 			=	re.findall(r'"max2":(.+?),', raw_data)[0]
	max3 			=	re.findall(r'"max3":(.+?),', raw_data)[0]
	max4			=	re.findall(r'"max4":(.+?),', raw_data)[0]
	max				=	re.findall(r'"max":(.+?)}', raw_data)[0]
	ajaxToken		=	re.findall(r"window.ajaxToken = '(.+?)'", page_content)[0]

	data			=  {'cmd' 					:	cmd,
						'defaultValues[tid]' 	:	tid,
						'defaultValues[btyp]'	:	btyp,
						'defaultValues[r1]'		:	r1,
						'defaultValues[r2]'		:	r2,
						'defaultValues[r3]'		:	r3,
						'defaultValues[r4]'		:	r4,
						'defaultValues[supply]'	:	supply,
						'defaultValues[pzeit]'	:	pzeit,
						'defaultValues[max1]'	:	max1,
						'defaultValues[max2]'	:	max2,
						'defaultValues[max3]'	:	max3,
						'defaultValues[max4]'	:	max4,
						'defaultValues[max]'	:	max,
						'ajaxToken'				:	ajaxToken}

	return data

def get_trade_data_2(page_content, ajaxToken):
	# page_content = page_content.replace('\\n','\n')
	# page_content = page_content.replace('\\t','\t')
	# page_content = page_content.replace('\\"','\"')
	# page_content = page_content.replace('\\/','/')

	cmd		=	'exchangeResources'
	did		= 	'91248'
	m20		=	re.findall(r'id="m2\[0.+?value="(.+?)"', page_content, re.DOTALL)[0]
	m21		=	re.findall(r'id="m2\[1.+?value="(.+?)"', page_content, re.DOTALL)[0]
	m22		=	re.findall(r'id="m2\[2.+?value="(.+?)"', page_content, re.DOTALL)[0]
	m23		=	re.findall(r'id="m2\[3.+?value="(.+?)"', page_content, re.DOTALL)[0]
	
	data 		=	{	'cmd'			: cmd,
						'did'			: did,
						'desired[0]'	: m20,
						'desired[1]'	: m21,
						'desired[2]'	: m22,
						'desired[3]'	: m23,
						'ajaxToken'		: ajaxToken}

	return data

# with open('out.html', 'r') as f:
# 	page_content 	= 	f.read()

# with open('out.html', 'w') as f:
# 	
# 	f.write(page_content)

# with open('out.html') as f:
# 	page_content	=	f.read()
	

# 	}
	# f.write(page_content)
	# m20				=	re.findall(r'id=\\"m2[0]\\" size=\\"5\\" maxlength=\\"10\\"\\n\\t\\t\\t\\t       value=\\"(.+?)\\"', page_content)

	# print(m20)

# sample = 'this is a sample string, it contains some characters\nasd'
# # result = re.findall(r'[a-z]*i[a-z]*', sample)
# result = re.findall(r'(?<=, ).*(?=\n)',sample) #['it contains some characters']

